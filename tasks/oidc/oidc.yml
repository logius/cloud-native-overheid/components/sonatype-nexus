#
# Create OIDC client in KeyCloak. 
#
- name: Gather facts for Oauth Proxy secret (maintained by Helm chart)
  k8s_info:
    api_version: v1
    kind: Secret
    name: oauth2-proxy
    namespace: "{{ config.nexus.namespace }}"
  register: oauth2_proxy_secret
  no_log: true

- name: Get OIDC client and cookie secret from OAuth Proxy secret
  set_fact:
    oidc_client_secret: "{{ oauth2_proxy_secret.resources[0].data['client-secret'] | b64decode }}"
    oidc_cookie_secret: "{{ oauth2_proxy_secret.resources[0].data['cookie-secret'] | b64decode }}"
  when: oauth2_proxy_secret.resources | length > 0
  no_log: true

- name: Create OIDC client and cookie secret when missing
  set_fact:
    oidc_client_secret: "{{ lookup('password', '/dev/null length=20 chars=ascii_letters') }}"
    oidc_cookie_secret: "{{ lookup('password', '/dev/null length=20 chars=ascii_letters') }}"
  when: oauth2_proxy_secret.resources | length == 0
  no_log: true

- name: Set client id 
  set_fact:
    client_id: "{{ config.system.cluster_name }}-nexus"

- name: Create or update Keycloak client
  include_role:
    name: common/keycloak
    tasks_from: keycloak_client.yml
  vars: 
    clientId: "{{ client_id }}"
    client_definition:
      clientId: "{{ client_id }}"
      name: "{{ client_id }}"
      baseUrl: "https://{{ config.nexus.fqdn }}"
      redirectUris: [ "https://{{ config.nexus.fqdn }}/oauth2/callback" ]
      secret: "{{ oidc_client_secret }}"
      fullScopeAllowed: true
      serviceAccountsEnabled: true
      defaultClientScopes: 
        - "profile"
        - "roles"
        - "email"
      protocolMappers:      
        # Username Mapper - the field 'sub' in the JWT will contain the username. 
        - config:
            claim.name: "sub"
            access.token.claim: False
            id.token.claim: True
            userinfo.token.claim: True
            user.attribute: "username"
            jsonType.label: "String"
            full.path: True         
          name: username
          consentRequired: false
          protocol: openid-connect
          protocolMapper: "oidc-usermodel-property-mapper"

        # Groups Mapper. 
        - config:
            full.path: True
            id.token.claim: True
            access.token.claim: False
            claim.name: "groups"
            userinfo.token.claim: True
          name: groups
          consentRequired: false
          protocol: openid-connect
          protocolMapper: "oidc-group-membership-mapper"

- name: Oauth Proxy 
  include_tasks: oauth_proxy.yml 

- name: Keycloak Nexus Plugin 
  include_tasks: keycloak-nexus-plugin.yml 
  