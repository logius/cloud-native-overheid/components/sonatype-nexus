# Initial Configuration tasks for Nexus, to be run when Nexus is ready to accept requests. 
 
- name: Get Nexus admin password
  include_tasks: admin/admin_password.yml

- name: Gather facts for Admin secret
  block: 
    - k8s_secret_info:
        name: nexus-admin
        namespace: "{{ config.nexus.namespace }}"
      register: nexus_admin_secret
      no_log: true

    - set_fact:
        nexus_admin_password: "{{ nexus_admin_secret['password'] }}"

- name: Create admin role
  include_tasks: admin/admin_role.yml

- name: Create Blobstores 
  include_tasks: create_blobstore.yml 
  loop_control:
    loop_var: blobstore 
  loop: 
    - maven-snapshots
    - maven-releases

- name: Configure realms
  uri:
    url: "https://{{ config.nexus.fqdn }}/provision/rest/v1/security/realms/active"
    method: PUT
    user: admin
    force_basic_auth: true
    password: "{{ nexus_admin_password }}"
    body_format: json
    body: 
      - "NexusAuthenticatingRealm"
      - "NexusAuthorizingRealm"
      - "org.github.flytreeleft.nexus3.keycloak.plugin.KeycloakAuthenticatingRealm"
    status_code: "204" 

- name: Configure Https proxy 
  block: 
    - name: Get Internet proxy host and port 
      set_fact: 
        # strip protocol part 
        proxy_host: "{{ cloudinfra.internet_proxy.HTTP_PROXY | regex_replace('.*\/(.*):(.*)', '\\1') }}"
        proxy_port: "{{ cloudinfra.internet_proxy.HTTP_PROXY | regex_replace('.*\/(.*):(.*)', '\\2') }}"
      
      # `extdirect` is an api used in the Nexus console. Not sure this call will be supported in newer versions of Nexus. 
    - name: Set Proxy 
      uri:
        url: "https://{{ config.nexus.fqdn }}/provision/extdirect"
        method: POST
        user: admin
        force_basic_auth: true
        password: "{{ nexus_admin_password }}"
        body_format: json
        body: 
          tid: 1
          type: rpc
          action: "coreui_HttpSettings"
          method: "update"
          data: 
            - httpsEnabled: true
              httpsHost: "{{ proxy_host }}"
              httpsPort: "{{ proxy_port }}"
        status_code: 200
  when: cloudinfra.internet_proxy is defined 

- name: Proxy Certificates 
  uri: 
    url: "https://{{ config.nexus.fqdn }}/provision/rest/v1/security/ssl/truststore"
    method: POST
    user: admin
    force_basic_auth: true
    password: "{{ nexus_admin_password }}"
    body_format: json
    body: "{{ cloudinfra.internet_proxy.certificates }}"
    status_code: "201, 409"
  when: cloudinfra.internet_proxy.certificates is defined 

- name: Configure anonymous readonly access 
  uri: 
    url: "https://{{ config.nexus.fqdn }}/provision/rest/v1/security/anonymous"
    method: PUT
    user: admin
    force_basic_auth: true
    password: "{{ nexus_admin_password }}"
    body_format: json
    body: 
      enabled:  "{{ config.nexus.anonymous_access | default(true) }}"
      userId: "anonymous" 
      realmName: "NexusAuthorizingRealm" 
    status_code: 200
