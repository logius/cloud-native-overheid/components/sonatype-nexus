# Migrating data from Nexus PVC.

# A backup of the Nexus data is made to a temporary PVC. De old PVC is subsequently removed.
# A reinstall of Nexus triggers the creation of a new PVC and then the backup data is copied back to the Nexus data directory.

# Note a Nexus backup consists of two backup jobs: BLOBS and databases. The tasklist below performs a simplified
# backup/restore by copying all data from nexus-data. If there is a lot of data in the Nexus store the two step
# backup proces as adviced by Nexus should be considered.

# Steps:
# 1. Destroy Nexus Deployment (but keep PVC)
# 2. Create a job with mount on Nexus data PVC to backup data to a temp Backup PVC
# 3. Destroy the Nexus PVCs
# 4. Rebuild Nexus to get new PVCs
# 5. Scale down Nexus to zero
# 6. Create a job with mount on Nexus data PVC to restore data from temp Backup PVC
# 7. Restart Nexus
# 8. Destroy backup PVC.

- name: Destroy Nexus (so we can make proper backup and destroy PVCs)
  k8s:
    state: absent
    api_version: v1
    kind: Deployment
    name: nexus-sonatype-nexus
    namespace: "{{ config.nexus.namespace }}"
    wait: true

- name: Create Temp Backup PVC
  k8s:
    state: present
    wait: true
    definition:
      apiVersion: v1
      kind: PersistentVolumeClaim
      metadata:
        name: nexus-temp-backup
        namespace: "{{ config.nexus.namespace }}"
      spec:
        storageClassName: "{{ config.nexus.persistence.storageClass | default(omit) }}"
        accessModes:
        - ReadWriteOnce
        resources:
          requests:
            storage: 8Gi

- name: Backup Data
  include_tasks: run_job.yml
  vars:
    job_name: "backup-job"
    job_command: ['sh', '-c', 'cp -arv /nexus-data/. /backup/' ]

- name: Find PVCs
  k8s_info:
    kind: PersistentVolumeClaim
    namespace: "{{ config.nexus.namespace }}"
    label_selectors:
      - app = sonatype-nexus
  register: pvcs

- name: Delete PVCs
  k8s:
    kind: PersistentVolumeClaim
    name: "{{ item.metadata.name }}"
    state: absent
    namespace: "{{ config.nexus.namespace }}"
    wait: true
  loop: "{{ pvcs.resources }}"

- name: Reinstall Nexus
  include_tasks: install_nexus.yml

- name: Shutdown Nexus (required for Restore)
  k8s_scale:
    api_version: v1
    kind: Deployment
    name: nexus-sonatype-nexus
    namespace: "{{ config.nexus.namespace }}"
    replicas: 0
    wait: false # wait flag fails, workaround see below

- name: Wait till Nexus is down
  k8s_info:
    api_version: v1
    kind: Pod
    namespace: "{{ config.nexus.namespace }}"
    label_selectors:
      - app = sonatype-nexus
  register: deployments
  until: deployments.resources | length == 0
  retries: 60
  delay: 2

- name: Restore Data
  include_tasks: run_job.yml
  vars:
    job_name: "restore-job"
    job_command: ['sh', '-c', 'rm -rf /nexus-data/* && cp -arv /backup/. /nexus-data']

- name: Finally Restart Nexus
  k8s_scale:
    api_version: v1
    kind: Deployment
    name: nexus-sonatype-nexus
    namespace: "{{ config.nexus.namespace }}"
    replicas: 1
    wait: no

- name: Delete Backup PVC
  k8s:
    kind: PersistentVolumeClaim
    name: nexus-temp-backup
    state: absent
    namespace: "{{ config.nexus.namespace }}"
    wait: true
