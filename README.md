# Nexus

In CNO, [Nexus](https://www.sonatype.com/nexus/repository-oss) is used to proxy and store software artifacts, such as Java archives.

__Platform Administrators__ use Nexus for custom platform installation libraries (not used in CNO at this stage).

__Application Developers__ use Nexus to proxy artifact retrieval from internet, and to store artifacts for re-use over multiple projects.

# Contents

This project defines an Ansible role for deploying Nexus.

Prerequisites:

- [CertManager](https://gitlab.com/logius/cloud-native-overheid/components/certmanager)
- [Nginx Ingress Controller](https://gitlab.com/logius/cloud-native-overheid/components/nginx-ingress)
- [Keycloak](https://gitlab.com/logius/cloud-native-overheid/components/keycloak)

In this project:

- Local installation, test and development (all optional)
- Datacenter installation
- Design notes

Local installation assumes the [platform-runner](https://gitlab.com/logius/cloud-native-overheid/components/platform-runner) to run in a Docker image.

# Local installation

Get platform runner:
```
docker pull registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest
```

Connect to Kubernetes cluster, e.g.:
```
export KUBECONFIG=$HOME/.kube/config
```

In working directory, add or extend `cluster_config.yaml`:
```
platform:
  prefix: "local"
  domain: example.com # Use your own domain, ref. certmanager

config:

  certmanager:
    domain:  example.com
    issuer: example-issuer # Use your own domain prefix instead of "example"

  keycloak:
    namespace: local-keycloak
    fqdn: keycloak.example.com
    realm: platform # Binds to custom CNO realm in Keycloak

  system:
    cluster_name: local
    admin_role: platform_beheerder # Binds to custom CNO role in Keycloak
    validate_certs: true
    hide_secrets_from_log: true # Use false for debugging
```

From working directory, install Nexus:
```
# Prepare Ansible command, assuming above cluster_config.yaml will be mounted below
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/nexus \
  -vv"

# Run Ansible role, using ANSIBLE_CMD with mounted cluster config
docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

The installation uses dependencies as defined in `./meta/main.yml`.

The reading order for Ansible parameterization is: `vars/main.yml`, overlayed with `$PWD/cluster_config.yaml`.

# Local test

Smoke test, unit test, integration test:

## Smoke test

Check deployment:
```
kubectl get pods --namespace local-nexus'
```

## Unit test

Perform the earlier `docker run`, now using:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/nexus \
  -t test \
  -vv"
```

## Integration test

Scope:

- As Keycloak admin in Keycloak: add a user of role platform administrator.
- As platform administrator, authenticate at Nexus via Keycloak.

Steps:

As Keycloak admin, [login](https://gitlab.com/logius/cloud-native-overheid/components/keycloak) at Keycloak.

- Observe Nexus is added as Client, with callback (Base) URL to the `Platform` realm.
- Create a platform administrator user (platform beheerder user), who will login at Nexus later on:

  - Create user: (`username, email verified`) -> save
  - Credentials: (`password, not temporary`)
  - Role mappings: (`assign platform_beheerder_role`) -> save

Add Nexus ip# and hostname to `/etc/hosts`, e.g.:
```
export WORKER_IP=$(kubectl get nodes -o wide | grep "worker .*Ready" | awk '{print $6}'); echo ${WORKER_IP}
sudo echo "${WORKER_IP} nexus.example.com" | sudo tee -a /etc/hosts # Use your own domain
```

As platform administrator in browser, login at Nexus via Keycloak:
```
https://nexus.example.com # Use your own domain
```
At first login, configure OTP when asked for, e.g. by using [Free OTP](https://freeotp.github.io/).

# Local development

Checkout this project, e.g.:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/nexus.git
```

Checkout projects for dependent roles as defined in `$PWD/nexus/meta/main.yml`:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/common.git
```

Develop and install, this time with `ANSIBLE_ROLES_PATH` and `galaxy_url` referring to local directory:
```
export ANSIBLE_ROLES_PATH=$PWD

ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=nexus \
  -vv"

docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -v $ANSIBLE_ROLES_PATH:/playbook/roles \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

Optional cleanup using:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=nexus \
  -t remove \
  -vv"
```

# Datacenter installation

In your own CI/CD, create a pipeline similar to the local installation.

Use a specific tag for `platform-runner`.

# Design notes

Content:

- Architecture
- Components
- Admin password
- Admin roles
- REST APIs

## Architecture

Nexus does not support OpenIDConnect. The Nexus domain is protected with an OAuth proxy, that diverts the user to Keycloak. To make this work, Nexus is configured with a Keycloak plugin.

```diagram
      Nginx        <-->     Nexus3
        |                      |
    OAuthProxy            Keycloak plugin
      |      |
  Keycloak  Redis
```

OAuthProxy and Keycloak are via the same Ingress controller. Redis is not accessible with an Ingress.

Ref.

- Auth Request Module: https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-subrequest-authentication/
- OAuth proxy: https://github.com/pusher/oauth2_proxy
- Keycloak plugin: https://github.com/flytreeleft/nexus3-keycloak-plugin

## Components

| Component | Description |
| :--- | :---- |
| Nginx | NGinx Ingress works with OAuthProxy in Auth Request mode. In this mode, the oauth proxy does not sit between the Ingress controller and the application. Instead the oauth proxy is accessed with sub requests coming from the NGinx Ingress controller. This mode is well suited for OIDC. For details of the configuration see nexus_oauth_ingress.yml and oauth2_proxy_values.yml |
| OAuthProxy | The proxy handles the OIDC communication with Keycloak. It sends the redirect to the browser so the user is required to login with Keycloak. After succesfull login, the OAuth proxy gets the access token from Keycloak. The ID token is not used with Nexus. The proxy keeps track of a cookie to know if a user has already logged in before.
| Redis | The cookies from the OAuth proxy may become too big, because the tokens (Access, Id, Refresh) are stored in the cookie. This is resolved by configuring a Redis backend. The cookie is now a small reference to the data in Redis. The OAuth Proxy has built-in support for Redis which makes the configuration quite easy.
| Keycloak | An OIDC client is configured in Keycloak. The client has a service account that is used by the Keycloak plugin in Nexus. The client has mappers for email and email_verified, to make sure email_verified is always True. The Keycloak plugin in Nexus denies access if the email is not verified.
| Keycloak plugin | The Keycloak plugin gets the OIDC access token from the OAuth proxy in an HTTP header and uses the token to query Keycloak for more information. The results are that the plugin (and Nexus) knows the user details and the users authorization with regard to groups. <br/> The plugin is a small Java application that is installed in Nexus. A Dockerfile is available to build Nexus with this plugin: https://git.rijksapps.nl/cno/standaardplatform/basis/nexus3-keycloak. <br/>The plugin requires a service account in Keycloak in order to query users and view groups. The credentials for this service account are configured in the file 'keycloak.json' which is provided to the Nexus pod as a mounted secret.

## Admin password

On first boot, Nexus generates an admin password and stores the password in the file `/nexus-data/admin.password`.

As long as this file exists, Nexus will prompt the admin users to change the admin password.

In this Ansible role the password is reset and saved to a K8s secret. The password is subsequently removed from Nexus, so the administrator is not prompted anymore for a reset of the password.

## Admin roles

An admin role with super powers is created for the group specified in 'config.system.admin_role'. A user that has been assigned to this group in Keycloak will have administrator privileges.

## REST APIs

Nexus is configured with REST APIs for user and role management, see https://help.sonatype.com/repomanager3/rest-and-integration-api.

Swagger: `https://nexus.example.com/service/rest/swagger.json`

## Ingress

Several ingresses are required.

| Ingress    | Description                                                                                    |
|:---------- |:---------------------------------------------------------------------------------------------- |
| Basic Auth | A basic auth ingress, required for command lines tools that cannot handle OIDC, such as maven. |
| OAuth      | The OAuth ingress helps the user for accessing the Nexus console using OIDC.                   |
| Provision  | The Provision ingress is created for Ansible tasks that need to access APIs in Nexus.          |

# Todo

Check cookie expiry.
